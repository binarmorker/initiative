import durationUnits from "./durationUnits"

export default [
  {
    id: 1,
    name: 'blinded',
    color: 'grey',
    dark: true,
    duration: ''
  },
  {
    id: 2,
    name: 'charmed',
    color: 'red',
    duration: '',
    source: ''
  },
  {
    id: 3,
    name: 'deafened',
    color: 'grey',
    dark: true,
    duration: ''
  },
  {
    id: 4,
    name: 'fatigued',
    color: 'blue',
    duration: ''
  },
  {
    id: 5,
    name: 'grappled',
    color: 'yellow',
    duration: '',
    source: ''
  },
  {
    id: 6,
    name: 'incapacitated',
    color: 'grey',
    dark: true,
    duration: ''
  },
  {
    id: 7,
    name: 'invisible',
    color: 'blue',
    duration: ''
  },
  {
    id: 8,
    name: 'paralyzed',
    color: 'yellow',
    duration: ''
  },
  {
    id: 9,
    name: 'petrified',
    color: 'grey',
    dark: true
  },
  {
    id: 10,
    name: 'poisoned',
    color: 'green',
    duration: ''
  },
  {
    id: 11,
    name: 'prone',
    color: 'blue'
  },
  {
    id: 12,
    name: 'restrained',
    color: 'green',
    duration: '',
    source: ''
  },
  {
    id: 13,
    name: 'stunned',
    color: 'yellow',
    duration: ''
  },
  {
    id: 14,
    name: 'unconscious',
    color: 'grey',
    dark: true
  },
  {
    id: 15,
    name: 'exhausted',
    color: 'blue',
    exhaustion: 0
  },
  {
    id: 16,
    name: 'dying',
    color: 'grey',
    dark: true,
    deathSaves: [0, 0]
  },
  {
    id: 17,
    name: 'dead',
    color: 'grey',
    dark: true
  },
  {
    id: 18,
    name: 'panicked',
    color: 'red',
    duration: ''
  },
  {
    id: 19,
    name: 'confused',
    color: 'red',
    duration: ''
  },
  {
    id: 20,
    name: 'frightened',
    color: 'blue',
    duration: '',
    source: ''
  },
  {
    id: 21,
    name: 'silenced',
    color: 'grey',
    dark: true,
    duration: ''
  },
  {
    id: 22,
    name: 'surprised',
    color: 'red',
    duration: '1',
    durationUnit: durationUnits.find(x => x.id === 'round')
  },
  {
    id: 23,
    name: 'shaken',
    color: 'red',
    duration: ''
  },
  {
    id: 24,
    name: 'rage',
    color: 'red',
    duration: ''
  },
  {
    id: 25,
    name: 'concentration',
    color: 'blue',
    duration: '',
    concentrationSave: true
  },
  {
    id: 26,
    name: 'inspiration',
    color: 'green',
    dice: '',
    duration: ''
  },
  {
    id: 27,
    name: 'advantage',
    color: 'grey',
    dark: true,
    duration: ''
  },
  {
    id: 28,
    name: 'disadvantage',
    color: 'grey',
    dark: true,
    duration: ''
  },
  {
    id: 29,
    name: 'hidden',
    color: 'blue'
  },
  {
    id: 30,
    name: 'cursed',
    color: 'grey',
    dark: true,
    duration: '',
    effect: ''
  },
  {
    id: 31,
    name: 'hexxed',
    color: 'grey',
    dark: true,
    duration: '',
    effect: ''
  },
  {
    id: 32,
    name: 'cover_1_2',
    color: 'green'
  },
  {
    id: 33,
    name: 'cover_3_4',
    color: 'green'
  },
  {
    id: 34,
    name: 'cover_full',
    color: 'green'
  },
  {
    id: 35,
    name: 'bonus',
    color: 'yellow',
    source: '',
    effect: '',
    duration: '',
    dice: ''
  },
  {
    id: 36,
    name: 'penalty',
    color: 'yellow',
    source: '',
    effect: '',
    duration: '',
    dice: ''
  },
  {
    id: 37,
    name: 'bleeding',
    color: 'red',
    duration: '',
    source: ''
  },
  {
    id: 38,
    name: 'broken',
    color: 'yellow',
    duration: '',
    source: ''
  },
  {
    id: 39,
    name: 'clumsy',
    color: 'blue',
    duration: '',
    source: ''
  },
  {
    id: 40,
    name: 'diseased',
    color: 'grey',
    dark: true,
    duration: '',
    source: ''
  },
  {
    id: 41,
    name: 'doomed',
    color: 'red',
    duration: '',
    source: ''
  },
  {
    id: 42,
    name: 'drained',
    color: 'blue',
    duration: '',
    source: ''
  },
  {
    id: 43,
    name: 'encumbered',
    color: 'green',
    duration: '',
    source: ''
  },
  {
    id: 44,
    name: 'weakened',
    color: 'yellow',
    duration: '',
    source: ''
  },
  {
    id: 45,
    name: 'wounded',
    color: 'red',
    duration: '',
    source: ''
  },
]
