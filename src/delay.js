let delayTime = 0

export function updateDelay (time) {
  delayTime = time
}

export function delay(callback) {
  let timer = 0

  return function(...args) {
    clearTimeout(timer)
    timer = setTimeout(() => {
      callback.apply(this, args)
    }, parseInt(delayTime) || 0)
  }
}
