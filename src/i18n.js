import Vue from 'vue'
import VueI18n from 'vue-i18n'
import messages from './messages.json'

Vue.use(VueI18n)

export default new VueI18n({
  locale: (navigator.language || navigator.userLanguage).split('-')[0],
  fallbackLocale: 'en',
  messages
})
