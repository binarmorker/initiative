import EditionView from './views/EditionView'
import PresentationView from './views/PresentationView'
import VueRouter from 'vue-router'
import Vue from 'vue'

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: EditionView
    },
    {
      path: '/view',
      component: PresentationView
    },
  ],
})
