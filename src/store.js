import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'
import i18n from './i18n'
import PubSub from 'browser-pubsub'
import { delay, updateDelay } from './delay'

export const initializeSync = 'initializeSync'
export const forceSync = 'forceSync'
export const getState = 'getState'

export const getRound = 'getRound'
export const setRound = 'setRound'

export const getCreatures = 'getCreatures'
export const getOrderedCreatures = 'getOrderedCreatures'
export const addCreature = 'addCreature'
export const editCreature = 'editCreature'
export const removeCreature = 'removeCreature'
export const activateCreature = 'activateCreature'

export const getParties = 'getParties'
export const addParty = 'addParty'
export const editParty = 'editParty'
export const replaceParty = 'replaceParty'
export const removeParty = 'removeParty'
export const setParty = 'setParty'

export const getSettings = 'getSettings'
export const setSettings = 'setSettings'

Vue.use(Vuex)

function removePrecisionError(number) {
  return parseFloat(number).toPrecision(5).replace(/[.0]+$/, '')
}

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})

const events = {
  [setRound]: new PubSub(setRound),
  [addCreature]: new PubSub(addCreature),
  [editCreature]: new PubSub(editCreature),
  [removeCreature]: new PubSub(removeCreature),
  [activateCreature]: new PubSub(activateCreature),
  [addParty]: new PubSub(addParty),
  [editParty]: new PubSub(editParty),
  [removeParty]: new PubSub(removeParty),
  [setParty]: new PubSub(setParty),
  [setSettings]: new PubSub(setSettings),
  [forceSync]: new PubSub(forceSync),
}

const prefersDarkTheme = window.matchMedia("(prefers-color-scheme: dark)")

export default new Vuex.Store({
  plugins: [ vuexLocal.plugin ],
  state: {
    round: 0,
    creatures: [],
    parties: [],
    settings: {
      language: i18n.locale,
      darkTheme: prefersDarkTheme.matches,
      updateDelay: 300,
      twentyFourHour: false,
      hideName: true,
      hideHP: true,
      hideStatus: false,
      autoStatusOnHpDepleted: null,
      autoStatusOnEnergyDepleted: null,
      hideDeathSaves: true,
      currentParty: null
    },
  },
  getters: {
    [getRound] (state) {
      return state.round || 0
    },
    [getCreatures] (state) {
      return state.creatures || []
    },
    [getOrderedCreatures] (state) {
      return state.creatures.slice().sort((a, b) => b.order - a.order) || []
    },
    [getParties] (state) {
      return state.parties.slice().sort((a, b) => a.id - b.id) || []
    },
    [getSettings] (state) {
      return state.settings
    },
    [getState] (state) {
      return state
    },
  },
  mutations: {
    [setRound] (state, payload) {
      state.round = payload.round
    },
    [addCreature] (state, payload) {
      state.creatures = [...state.creatures, payload.creature]
    },
    [editCreature] (state, payload) {
      state.creatures = [
        ...state.creatures.filter(creature => creature.id !== payload.creature.id),
        payload.creature
      ]
    },
    [removeCreature] (state, payload) {
      state.creatures = state.creatures.filter(creature => creature.id !== payload.creature.id)
    },
    [activateCreature] (state, payload) {
      if (~state.creatures.findIndex(creature => creature.id === payload.creatureId)) {
        const otherCreatures = state.creatures
          .filter(creature => creature.id !== payload.creatureId)
          .map(creature => ({ ...creature, active: false }))
        const currentCreature = { ...state.creatures.find(creature => creature.id === payload.creatureId), active: true }
        const previousCreature = state.creatures.find(creature => creature.id === payload.currentCreatureId)
        const counterCreature = payload.isPrevious ? previousCreature : currentCreature
        
        if (counterCreature) {
          for (let i in counterCreature.status) {
            for (let j in counterCreature.status[i].states) {
              if (counterCreature.status[i].states[j].durationUnit) {
                const durationIncrement = 6 / counterCreature.status[i].states[j].durationUnit.seconds

                if (payload.isPrevious) {
                  counterCreature.status[i].states[j].duration = removePrecisionError(+counterCreature.status[i].states[j].duration + durationIncrement)
                } else {
                  counterCreature.status[i].states[j].duration = removePrecisionError(+counterCreature.status[i].states[j].duration - durationIncrement)
                }
              }

              if (counterCreature.status[i].states[j].duration <= 0) {
                counterCreature.status[i].states[j].duration = 0
              }
            }
          }
        }

        if (!payload.isPrevious && currentCreature) {
          for (let i in currentCreature.status) {
            if (!currentCreature.status[i].reaction) {
              currentCreature.status[i].reaction = true
            }
          }
        }

        state.creatures = [
          ...otherCreatures,
          currentCreature
        ]
      } else {
        state.creatures = state.creatures.map(creature => ({ ...creature, active: false }))
      }
    },
    [addParty] (state, payload) {
      state.parties = [ ...state.parties, payload.party ]
    },
    [editParty] (state, payload) {
      state.parties = [
        ...state.parties.filter(party => party.id !== payload.party.id),
        payload.party
      ]
    },
    [removeParty] (state, payload) {
      state.parties = state.parties.filter(party => party.id !== payload.party.id)
    },
    [setParty] (state, payload) {
      state.creatures = payload.party.creatures
      state.settings.currentParty = payload.party.id
    },
    [replaceParty] (state, payload) {
      state.parties = [
        ...state.parties.filter(party => party.id !== payload.party.id),
        payload.party
      ]
    },
    [setSettings] (state, payload) {
      state.settings = payload.settings
      updateDelay(state.settings.updateDelay)
    },
    [forceSync] (state, payload) {
      if (payload) {
        state.creatures = payload.creatures
        state.parties = payload.parties
        state.settings = payload.settings
        state.round = payload.round
      }
      updateDelay(state.settings.updateDelay)
    }
  },
  actions: {
    [initializeSync] (context) {
      updateDelay(context.state.settings.updateDelay)
      events[setRound].subscribe(payload => context.commit(setRound, payload))
      events[addCreature].subscribe(payload => context.commit(addCreature, payload))
      events[editCreature].subscribe(payload => context.commit(editCreature, payload))
      events[removeCreature].subscribe(payload => context.commit(removeCreature, payload))
      events[activateCreature].subscribe(payload => context.commit(activateCreature, payload))
      events[addParty].subscribe(payload => context.commit(addParty, payload))
      events[editParty].subscribe(payload => context.commit(editParty, payload))
      events[removeParty].subscribe(payload => context.commit(removeParty, payload))
      events[setParty].subscribe(payload => context.commit(setParty, payload))
      events[setSettings].subscribe(payload => context.commit(setSettings, payload))
      events[forceSync].subscribe(payload => context.commit(forceSync, payload))
    },
    [setRound]: (_, payload) => events[setRound].publish(payload),
    [addCreature]: (_, payload) => events[addCreature].publish(payload),
    [editCreature]: delay((_, payload) => events[editCreature].publish(payload)),
    [removeCreature]: (_, payload) => events[removeCreature].publish(payload),
    [activateCreature]: (_, payload) => events[activateCreature].publish(payload),
    [addParty]: (_, payload) => events[addParty].publish(payload),
    [editParty]: delay((_, payload) => events[editParty].publish(payload)),
    [removeParty]: (_, payload) => events[removeParty].publish(payload),
    [setParty]: delay((_, payload) => events[setParty].publish(payload)),
    [setSettings]: (_, payload) => events[setSettings].publish(payload),
    [forceSync]: context => events[forceSync].publish(context.state)
  }
})
