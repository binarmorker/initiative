export default [
  {
    id: 'round',
    seconds: 6
  },
  {
    id: 'turn',
    seconds: 6
  },
  {
    id: 'second',
    seconds: 1
  },
  {
    id: 'minute',
    seconds: 60
  },
  {
    id: 'hour',
    seconds: 0
  },
  {
    id: 'day',
    seconds: 0
  }
]
